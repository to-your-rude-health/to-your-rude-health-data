#!/bin/sh
input_images="$1"
output_images="$2"
for i in "$input_images"/*; do
	bname=$(basename "$i")
	if expr "$(file -b $i)" : 'JPEG' > /dev/null; then
		cwebp -resize 832 0 -q 50 -m 6 "$i" -o "${output_images}/${bname%.*}.webp"
		convert "$i" -strip -thumbnail 832 -quality 50 "${output_images}/${bname%.*}.jpg"
	else
		cp "$i" "${output_images}/$bname"
	fi
done
