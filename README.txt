Dear catch adventurer,

So it's a python 3 program. You'll have to install python 3 NOT python 2 (idk
how on windows) as well as pip. Google for 'install pip windows' should tell you
(once you've installed python ofc).

Once pip is working, install the package "cli-ui", "fuzzywuzzy",
"python-Levenshtein" and "treepick" using a command like:

	pip install cli-ui fuzzywuzzy python-Levenshtein treepick

on the command prompt.

In the directory you're working from (google for some basic windows commands
like change directory), put the python program 'tyrh-input.py' in that directory
and type 'python tyrh-input.py'. You should get this ...

  usage: tyrh-input.py [-h] [-l] [-i IMAGES] FILE
  tyrh-input.py: error: the following arguments are required: FILE

... that's because we haven't supplied it with a JSON file yet. I have a JSON
file that I've started doing Farmer e2 with, but here I'll walk you through
inputting the first song of Farmer e9 (and include it in the JSON so you don't
have to do all this again (n.b. from the future, I actually did a few more than
that).

You can view the options of the program by running 'python tyrh-input.py -h'
which should tell you about the '-i' switch as well, which allows you to select
another directory which contains images.

First of all, you can list the songs and data currently in the file by running
'python tyrh-input.py -l tyrh.json' (that's a lowercase L for 'list') which
should show some tables of what is already input in the file, including a list
of songs.

So, starting with Farmer e9, we run the program this time supplying it with the
JSON file (in the same directory) on the command line, like this: 'python
tyrh-input.py tyrh.json'. You'll be presented with: ...

  New Song
  ---------

  :: Sources
     1 (Farmer e2) A collection of catches, canons and ...
     2 New
  >

... and a flashing cursor. We want to input a new source, so we'll hit '2' to
make a new source ...

  New Source
  -----------

  :: Shelfmark
  >

... the shelfmark for our new source is 'Farmer e9' so we'll type that in and
press enter ...

  New Source
  -----------

  :: Shelfmark
  > Farmer e9
  :: Title (one line)
  >

... here we input the title of the source edited into modern English, but with
ye olde contractions and ye olde spelling if possible. However, keep
capitalisation only to the beginning and proper nouns, and insert relevant
punctuation where necessary. Watch out throughout this for long 's' which always
looks too much like an 'f' to me ...

  New Source
  -----------

  :: Shelfmark
  > Farmer e9
  :: Title (one line)
  > A collection of vocal harmony consisting of catches, canons and glees never
    before publish'd to which are added several motetts and madrigals composed
    by the best masters selected by Thos. Warren
  :: Formatted Title (multiple lines)
  >

... now what we want is the title page exactly as formatted, including line
breaks and weird capitalisation, every single bit of text on the front page. An
empty line ends the input, so keep inputting lines and when you're done, just
input an empty line and it will finish ...

  New Source
  -----------

  :: Shelfmark
  > Farmer e9
  :: Title (one line)
  > A collection of vocal harmony consisting of catches, canons and glees never
    before publish'd to which are added several motetts and madrigals composed
    by the best masters selected by Thos. Warren
  :: Formatted Title (multiple lines)
  > A Collection of VOCAL HARMONY
  > consisting of
  > CATCHES CANONS and GLEES
  > never before publish'd
  > to which are added several
  > Motetts and Madrigals
  > Composed by the best Masters
  > selected by
  > THOS. WARREN
  > LONDON Printed by WELCKER in Gerrard Street, St. Ann's Soho
  :: Editors
  :: People
     1 John Travers (c1703 - 1758)
     2 Jonathan Battishill (1738 - 1801)
     3 Thomas Warren (c1730 - c1794)
     4 William Boyce (1711 - 1779)
     5 William Savage (1720 - 1789)
     6 New
  >

... now it is asking for the editor. We have some editors already input, and the
editor for this volume is Thomas Warren, so let's hit '3' to select him ...

  New Source
  -----------

  :: Shelfmark
  > Farmer e9
  :: Title (one line)
  > A collection of vocal harmony consisting of catches, canons and glees never
    before publish'd to which are added several motetts and madrigals composed
    by the best masters selected by Thos. Warren
  :: Formatted Title (multiple lines)
  > A Collection of VOCAL HARMONY
  > consisting of
  > CATCHES CANONS and GLEES
  > never before publish'd
  > to which are added several
  > Motetts and Madrigals
  > Composed by the best Masters
  > selected by
  > THOS. WARREN
  > LONDON Printed by WELCKER in Gerrard Street, St. Ann's Soho
  :: Editors
  :: People
     1 John Travers (c1703 - 1758)
     2 Jonathan Battishill (1738 - 1801)
     3 Thomas Warren (c1730 - c1794)
     4 William Boyce (1711 - 1779)
     5 William Savage (1720 - 1789)
     6 New
  > 3
  :: People
     1 John Travers (c1703 - 1758)
     2 Jonathan Battishill (1738 - 1801)
     3 * [Thomas Warren (c1730 - c1794)]
     4 William Boyce (1711 - 1779)
     5 William Savage (1720 - 1789)
     6 New
  >

... it asks again? This is normal, it will keep asking until you answer with
a blank, so you can input multiple editors this way. You can see that there's
a star beside Warren and his name is now in brackets. He is now selected. You
can unselect an option by selecting it again. There's only one editor to this
volume, so let's enter a blank and continue ...

  :: People
     1 John Travers (c1703 - 1758)
     2 Jonathan Battishill (1738 - 1801)
     3 * [Thomas Warren (c1730 - c1794)]
     4 William Boyce (1711 - 1779)
     5 William Savage (1720 - 1789)
     6 New
  >
  :: Are there images to this source not relating to pages (front matter etc.)?
     (y/N)
  >

... it's asking here whether there are images relating to the source that are
not part of a page, like front matter and titles and close ups of reference
cards, etc. Everything that isn't a picture of a song! We have some photos for
this source, so we hit 'y' and that will dump us into a file select dialog. This
is quite easy to work out. Use the arrow keys to move up and down, and use space
to select an image. When you've selected all the images, press 'q' to quit and
that will take you to...

  :: Sources
     1 (Farmer e2) A collection of catches, canons and ...
     2 * [(Farmer e9) A collection of vocal harmony consis...]
     3 New
  >

... it brings us back to the source selection menu. We can see now that our
newly-input source is in option '2' and it has been selected for us, so we want
that. All we need to do now is enter a blank to continue...

  => Choosing pages from Farmer e9
  :: Page number (empty for no more)
  >

... Now we must select the page(s) that this song resides on. However, Farmer e9
is a new source and has no pages input just yet, so we must input this page from
scratch. Our first song we're interested in is a canon that resides on pages
4 and 5 of Farmer e9, so we must input these pages n.b. if there is multiple
  pagination like in Farmer e9, use the foliation that is most consistent. In
  this case it is the inner numbers ...

  => Choosing pages from Farmer e9
  :: Page number (empty for no more)
  > 4
  :: Not found. Input new? (Y/n)
  > y
  New Page
  ---------
  :: Are there images for this page? (Y/n)
  >

  [ Complex file picking sequence here...not shown]

  => Currently chosen: 4
  :: Page number (empty for no more)
  > 5
  :: Not found. Input new? (Y/n)
  > y
  New Page
  ---------
  :: Are there images for this page? (Y/n)
  >

  [ Complex file picking sequence here...not shown]

  => Currently chosen: 4, 5
  :: Page number (empty for no more)
  >
  :: Text as presented here (multiple lines)
  >

... complex! So what happened here is first I searched for page '4' in the
database. It of course did not exist, so it asked me if I wanted to add it.
I typed 'y' for yes which added the page to the database, after prompting me to
select the images that pertain to this page. Then I did the same for page '5',
and the prompt changed to tell me that I currently had pages 4 and 5 selected
(n.b. if I wanted to deselect a page, I would search it again). Then, with all
these pages created and selected, I entered blank to continue where it now gives
me the option of inputting a text. Here we want to input the text exactly as
presented in the source, one line per part, weird capitalisation and everything
as written ...

  :: Edited text (multiple lines)
  > Hosanna in excelsis Hosanna in excelsis Hosanna Hosanna in excelsis in
    excelsis Hosanna in excelsis Hosanna in excelsis Hosanna
  > Hosanna in excelsis Hosanna in excelsis Hosanna Hosanna in excelsis in
    excelsis Hosanna in excelsis Hosanna in excelsis
  > Hosanna in excelsis Hosanna in excelsis Hosanna Hosanna in excelsis in
    excelsis Hosanna in excelsis Hosanna
  > Hosanna in excelsis Hosanna in excelsis Hosanna Hosanna in excelsis in
    excelsis Hosanna in excelsis
  >
  :: Texts
     1 Come ye jolly fellows, let us now be me...
     2 A hogshead was offered to Bacchus' shri...
     3 Ye birds for whom I reared this grove, ...
     4 Doubtless the pleasure is as great in b...
     5 Let us drink and be merry, dance, joke ...
     6 New
     7 More
     8 Exit

... it then searches the database for text matches to this. Obviously this is
not found as the text has not been entered yet! So we want to select '6' to
enter a new text ...

  New Text
  ---------

  :: Edited text (multiple lines)
  > Hosanna in excelsis.
  >
  :: Characters
     1 Bacchus
     2 Ralpho
     3 Claret
     4 The Devil
     5 Husband
     6 New
     7 More
     8 Exit
  >

... the text to this is simply 'Hosanna in excelsis', but if we had a poem with
multiple lines (i.e. multiple lines of a round or catch), then we'd enter them
as multiple lines. It also searches the database and prompts us for
'Characters'. I wish to catalogue the stock characters that appear in the songs,
so if a round or catch mentions a person, god, figure (real or fictional) by
name then input them the usual way here. This canon doesn't include any
characters, so we just enter blank. It then takes us back to the text selection
menu, where our new text is preselected. So let's just enter blank again to end
the selection ...

  :: Texts
     1 * [Hosanna in excelsis]
     2 Come ye jolly fellows, let us now be me...
     3 A hogshead was offered to Bacchus' shri...
     4 Ye birds for whom I reared this grove, ...
     5 Doubtless the pleasure is as great in b...
     6 New
     7 More
     8 Exit
  >
  :: Composers
  :: Composer as presented (including 'by ...')
  >

... In the source, it is written as being composed by 'Geo. Berg' so let's input
that exactly as written ...

  :: Composers
  :: Composer as presented (including 'by ...')
  > Geo. Berg
  :: Composers
  1 John Travers (c1783 - 1758)
  2 Samuel Webbe (1740 - 1816)
  3 Felice Giardini (1716 - 1796)
  4 Thomas Arne (1710 - 1778)
  5 James Nares (1715 - 1783)
  6 New
  7 More
  8 Exit

.. It searches the database for George Berg but can't find him because he hasn't
been input yet. This is composed by one George Berg (1730 - 1775), so let's
input him using option '6' ...

  New Person
  ----------

  :: Name
  > George Berg
  :: Birth year (if known). Prefix with 'c' if approximate
  > 1730
  :: Death year (if known). Prefix with 'c' if approximate
  > 1775
  :: Composers
  1 * [George Berg (1730 - 1775)]
  2 John Travers (c1783 - 1758)
  3 Samuel Webbe (1740 - 1816)
  4 Felice Giardini (1716 - 1796)
  5 Thomas Arne (1710 - 1778)
  6 James Nares (1715 - 1783)
  7 New
  8 More
  9 Exit
  >
  :: Genres
     1 Epitaph
     2 New
  >

... so here we input George Berg including dates and selected him as composer of
this canon. The genre is how the song is referenced IN THE SOURCE, don't make
any judgements yourself. This one is referenced as a canon, so let's make
a genre for canons ...

  :: Genres
     1 Epitaph
     2 New
  > 2
  New Genre
  ----------

  :: Name
  > Canon
  :: Genres
     1 * [Canon]
     2 Epitaph
     3 New
  >
  :: Title
  >

... a title is given if there is some distinguishing text that relates to the
song. In this case, it is 'Four in one', describing the canon. Let's edit this
text as before in one line. The next steps are fairly easy by this point. What
remains to say is format the key signature with a hyphen if it is flat or sharp,
and distinguish between major and minor ...

  :: Title
  > Four in one
  :: Number of voices
  > 4
  :: Keys
     1 A minor
     2 B-flat major
     3 C major
     4 D major
     5 F major
     6 F minor
     7 New
  > 7
  New Key
  --------

  :: Name
  > G major
  :: Keys
     1 A minor
     2 B-flat major
     3 C major
     4 D major
     5 F major
     6 F minor
     7 * [G major]
     8 New
  >
  :: Tempos
     1 Andante
     2 New
  > 2
  New Tempo
  ----------

  :: Name
  > Not too fast
  :: Tempos
     1 Andante
     2 * [Not too fast]
     3 New
  >
  :: Timesigs
     1 3/4
     2 6/8
     3 Common time
     4 Cut time
     5 New
  > 4
  :: Timesigs
     1 3/4
     2 6/8
     3 Common time
     4 * [Cut time]
     5 New
  >
  Wrote output to tyrh.json
  :: Same source and page? (y/N)
  >
  New Song
  ---------

  :: Sources
     1 (Farmer e2) A collection of catches, canons and ...
     2 (Farmer e9) A collection of vocal harmony consis...
     3 New
  >

... you can see that when it said 'wrote output', the file was saved. Any work
done before that would be lost if the program were to exit. Make regular
backups! Now I'm going to input the next one in the series (catch 'Beviamo
amici' on p.5) and show the output. Here it asks 'Same source and page?' type
'y' here to skip the questions about sources and pages if the song is on the
exact same set of pages in the exact same source (useful for many short catches
on one page).

Before we begin, a short word about submitting changes. If you make a gitlab.com
account, once you make changes to tyrh.json, you can click on tyrh.json in the
project repository, then on the blue button marked 'Edit'. Replace the file
contents with your new file's contents, change the Commit message to something
more descriptive than 'Update tyrh.json' and make a name for the target branch,
something like 'farmer-e9'. Make sure 'Start a new merge request' is ticked and
then press 'Commit changes'. Make sure you keep tyrh.json updated from the
repository master branch every time you do new work on the file ...

  :: Same source and page? (y/N)
  >
  New Song
  ---------

  :: Sources
     1 (Farmer e2) A collection of catches, canons and ...
     2 (Farmer e9) A collection of vocal harmony consis...
     3 New
  > 2
  :: Sources
     1 (Farmer e2) A collection of catches, canons and ...
     2 * [(Farmer e9) A collection of vocal harmony consis...]
     3 New
  >
  => Choosing pages from Farmer e9
  :: Page number (empty for no more)
  > 5
  => Currently chosen: 5
  :: Page number (empty for no more)
  >
  :: Text as presented here (multiple lines)
  > Beviamo amici beviam che il giorno presto e al ritorno presto al partir
  > di giovenezza godiam godiamo il fiore poi poi l'ultim'ore lasciam venir
  > e troppo ardente il Vin di spagna quel di sciampagna vogliam vogliam sentir
  > cosi brillante lo vuo il Francese chelo l'Inglese l'ama cosi
  >
  :: Texts
     1 'Tis thus, and thus farewell to all vain mortals...
     2 A hogshead was offered to Bacchus' shrine, the g...
     3 Here Innocence and Beauty lies whose breathe was...
     4 Hosanna in excelsis
     5 May the King live long, ding, dong, ding, dong, ...
     6 New
     7 More
     8 Exit
  > 6
  :: Edited text (multiple lines)
  > Beviamo amici, beviam che'il giorno, presto e'al ritorno presto al partir,
  > di giovenezza godiam godiamo il fiore poi, poi, l'ultim'ore lasciam venir,
  > e troppo ardente, il vin di spagna quel di sciampagna vogliam sentir,
  > cosi brillante lo vuo il Francese cheto l'Inglese l'ama cosi.
  >
  :: Characters
     1 Celia
     2 Eliza
     3 Asia
     4 Africa
     5 Britain
     6 New
     7 More
     8 Exit
  :: Texts
     1 * [Beviamo amici, beviam che'il giorno, presto e'al...]
     2 'Tis thus, and thus farewell to all vain mortals...
     3 A hogshead was offered to Bacchus' shrine, the g...
     4 Here Innocence and Beauty lies whose breathe was...
     5 Hosanna in excelsis
     6 New
     7 More
     8 Exit
  >
  :: Composers
  :: Composer as presented (including 'by ...')
  >
  :: Genres
     1 Canon
     2 Epitaph
     3 New
  > 3
  New Genre
  ----------

  :: Name
  > Catch
  :: Genres
     1 Canon
     2 * [Catch]
     3 Epitaph
     4 New
  >
  :: Title
  >
  :: Number of voices
  > 4
  :: Keys
     1 A minor
     2 B-flat major
     3 C major
     4 D major
     5 F major
     6 F minor
     7 G major
     8 New
  > 7
  :: Keys
     1 A minor
     2 B-flat major
     3 C major
     4 D major
     5 F major
     6 F minor
     7 * [G major]
     8 New
  >
  :: Tempos
     1 Andante
     2 Not too fast
     3 New
  > 3
  New Tempo
  ----------

  :: Name
  > Spirituoso
  :: Tempos
     1 Andante
     2 Not too fast
     3 * [Spirituoso]
     4 New
  >
  :: Timesigs
     1 3/4
     2 6/8
     3 Common time
     4 Cut time
     5 New
  > 4
  :: Timesigs
     1 3/4
     2 6/8
     3 Common time
     4 * [Cut time]
     5 New
  >
  Wrote output to tyrh.json
  :: Same source and page? (y/N)
  >
  New Song
  ---------

  :: Sources
     1 (Farmer e2) A collection of catches, canons and ...
     2 (Farmer e9) A collection of vocal harmony consis...
     3 New
  >
