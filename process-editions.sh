#!/bin/sh
INPUT_JSON="$1"
OUTPUT_PDF="$2"
tmpabc=$(mktemp)
jq -r '.editions[]|[.id, .abc]|@tsv' < "$INPUT_JSON" |
	while IFS=$'\t' read -r ed_id ed_abc; do
		echo "$ed_id"
		echo -e "$ed_abc" > "$tmpabc"
		abcm2ps -O - "$tmpabc" | ps2pdf - "$OUTPUT_PDF"/"$ed_id".pdf
	done
rm -f "$tmpabc"
