#!/usr/bin/env python3
import cli_ui
import json
import os
import argparse
import copy
import sys
import re
from fuzzywuzzy import process
import curses
from treepick import pick
images_path = './images/'
audio_path = './audio/'
class readable_dir(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        prospective_dir=values
        if not os.path.isdir(prospective_dir):
            raise argparse.ArgumentTypeError("readable_dir:{0} is not a valid path".format(prospective_dir))
        if os.access(prospective_dir, os.R_OK):
            setattr(namespace, self.dest, prospective_dir)
        else:
            raise argparse.ArgumentTypeError("readable_dir:{0} is not a readable dir".format(prospective_dir))
def fmt_len(string):
    try:
        cols, rows = os.get_terminal_size(0)
    except OSError as e:
        cols = 80
        rows = 64
    choicewidth = rows - 5
    string = string.replace('\r', ' ').replace('\n', ' ')
    append = ""
    if len(string) > choicewidth:
        append = "..."
        choicewidth -= 3
    return string[:choicewidth] + append
def fmt_source(src, select):
    string = ""
    if "shelfmark" in src:
        string += "(" + src["shelfmark"] + ") "
    if "title" in src:
        string += src["title"]
    string = fmt_len(string)
    if select:
        string = "* [" + string + "]"
    return string
def fmt_page(page, select):
    string = ""
    if "pagenum" in page and page["pagenum"]:
        string += page["pagenum"]
    string = fmt_len(string)
    if select:
        string = "* [" + string + "]"
    return string
def fmt_text(text, select):
    string = ""
    if "edited" in text:
        string += text["edited"]
    string = fmt_len(string)
    if select:
        string = "* [" + string + "]"
    return string
def fmt_character(character, select):
    string = ""
    if "name" in character:
        string += character["name"]
    string = fmt_len(string)
    if select:
        string = "* [" + string + "]"
    return string
def fmt_genre(genre, select):
    string = ""
    if "name" in genre:
        string += genre["name"]
    string = fmt_len(string)
    if select:
        string = "* [" + string + "]"
    return string
def fmt_key(key, select):
    string = ""
    if "name" in key:
        string += key["name"]
    string = fmt_len(string)
    if select:
        string = "* [" + string + "]"
    return string
def fmt_tempo(tempo, select):
    string = ""
    if "name" in tempo:
        string += tempo["name"]
    string = fmt_len(string)
    if select:
        string = "* [" + string + "]"
    return string
def fmt_timesig(timesig, select):
    string = ""
    if "name" in timesig:
        string += timesig["name"]
    string = fmt_len(string)
    if select:
        string = "* [" + string + "]"
    return string
def fmt_person(person, select):
    string = ""
    if "name" in person:
        string += person["name"]
    if ("dates" in person) and person["dates"]:
        string += ' (' + ' - '.join(person["dates"]) + ')'
    string = fmt_len(string)
    if select:
        string = "* [" + string + "]"
    return string
def ask_multiline(prompt):
    cli_ui.info_1(prompt)
    string = ""
    while True:
       tmp = cli_ui.read_input()
       if not tmp:
           break
       string += "\n" + tmp
    return string.strip()
def input_person(dat, info = {}):
    cli_ui.info_section("New Person")
    if 'name' not in info:
        info['name'] = cli_ui.ask_string("Name")
    if 'dates' not in info:
        birthdate = cli_ui.ask_string("Birth year (if known). Prefix with 'c' if approximate")
        deathdate = cli_ui.ask_string("Death year (if known). Prefix with 'c' if approximate")
        if birthdate:
            if deathdate:
                dates = [birthdate, deathdate]
            else:
                dates = [birthdate, "?"]
        else:
            if deathdate:
                dates = ["?", deathdate]
            else:
                dates = []
        info['dates'] = dates
    return info, dat
def input_images():
    list_of_paths = curses.wrapper(pick, images_path, True, True)
    return [os.path.basename(x) for x in list_of_paths]
def input_audio():
    list_of_audios = curses.wrapper(pick, audio_path, True, True)
    return os.path.basename(list_of_audios[0])
def input_source(dat, info = {}):
    cli_ui.info_section("New Source")
    if 'shelfmark' not in info:
        info['shelfmark'] = cli_ui.ask_string("Shelfmark")
    if 'title' not in info:
        info['title'] = cli_ui.ask_string("Title (one line)")
        if not info['title']:
            title = ""
    if 'formattedtitle' not in info:
        info['formattedtitle'] = ask_multiline("Formatted Title (multiple lines)")
    if 'editor_id' not in info:
        cli_ui.info_1("Editors")
        ce, dat = choose(dat, "people", fmt_person, input_person, "name", {})
        info['editor_id'] = get_ids(ce)
    if 'images' not in info:
        cli_ui.info_1("Images")
        if cli_ui.ask_yes_no("Are there images to this source not relating to pages (front matter etc.)?", default=False):
            info['images'] = input_images()
    return info, dat
def input_page(dat, info = {}):
    cli_ui.info_section("New Page")
    if 'source_id' not in info:
        cs, dat = choose(dat, "sources", fmt_source, input_source, "title", {})
        info["source_id"] = get_ids(cs)
    if 'pagenum' not in info:
        pagenum = cli_ui.ask_string("Page number")
        info["pagenum"] = pagenum
    if 'images' not in info:
        cli_ui.info_1("Images")
        if cli_ui.ask_yes_no("Are there images for this page?", default=True):
            info['images'] = input_images()
    return info, dat
def input_text(dat, info = {}):
    cli_ui.info_section("New Text")
    if 'edited' not in info:
        info['edited'] = ask_multiline("Edited text (multiple lines)")
    if 'character_id' not in info:
        cli_ui.info_1("Characters")
        new_obj = {"name": "New"}
        exit_obj = {"name": "Exit"}
        more_obj = {"name": "More"}
        lim = 5
        chosen = []
        passfmt = lambda a: fmt_character(a, True if a in chosen else False)
        while True:
            choice_characters = select_fuzzy(info["edited"], dat["characters"], "name", lim)
            for character in chosen:
                if character not in choice_characters:
                    choice_characters += [character]
            choice_characters += [new_obj, more_obj, exit_obj]
            t = cli_ui.ask_choice("Characters", choices=choice_characters, func_desc=passfmt, sort=False)
            if t == None:
                break
            if t in chosen:
                chosen.remove(t)
            elif t == new_obj:
                tmp, dat = input_character(dat, {})
                tmp["id"] = new_id(dat["characters"])
                dat["characters"] += [tmp]
                chosen += [tmp]
                choice_characters = [tmp] + choice_characters
            elif t == exit_obj:
                if cli_ui.ask_yes_no("Data input since last save will be lost.  Confirm exit?", default=False):
                    sys.exit()
                else:
                    continue
            elif t == more_obj:
                lim *= 2
                continue
            else:
                chosen += [t]
        info["character_id"] = get_ids(chosen)
    if 'warning_id' not in info:
        chosen_warnings, dat = choose(dat, "warnings", fmt_warning, input_warning, "name", {})
        info['warning_id'] = get_ids(chosen_warnings) 
    return info, dat
def input_character(dat, info = {}):
    cli_ui.info_section("New Character")
    if 'name' not in info:
        info['name'] = cli_ui.ask_string("Name")
    return info, dat
def input_genre(dat, info = {}):
    cli_ui.info_section("New Genre")
    if 'name' not in info:
        info['name'] = cli_ui.ask_string("Name")
    return info, dat
def input_key(dat, info = {}):
    cli_ui.info_section("New Key")
    if 'name' not in info:
        info['name'] = cli_ui.ask_string("Name")
    return info, dat
def input_tempo(dat, info = {}):
    cli_ui.info_section("New Tempo")
    if 'name' not in info:
        info['name'] = cli_ui.ask_string("Name")
    return info, dat
def input_timesig(dat, info = {}):
    cli_ui.info_section("New Time Signature")
    if 'name' not in info:
        info['name'] = cli_ui.ask_string("Name")
    return info, dat
def new_id(dat):
    ids = sorted(get_ids(dat))
    if not ids:
        return 1
    newid = int(ids[-1]) + 1
    return newid
def choose(dat, sub, fmt, inpt, new_key, filt, sort=None):
    new_obj = {new_key: "New"}
    exit_obj = {new_key: "Exit"}
    chosen = []
    while True:
        cpy = copy.deepcopy(dat)
        i = cpy[sub]
        for k, v in filt.items():
            i = [a for a in i if ((k in a) and ((a[k] == v) or (v in a[k])))]
        if sort:
            sortfmt = lambda a: sort(a)
        else:
            sortfmt = lambda a: fmt(a, False)
        i = sorted(i, key=sortfmt)
        i += [new_obj]
        i += [exit_obj]
        passfmt = lambda a: fmt(a, True if a in chosen else False)
        t = cli_ui.ask_choice(sub.capitalize(), choices=i, func_desc=passfmt, sort=False)
        if t == None:
            break
        if t in chosen:
            chosen.remove(t)
        elif t == new_obj:
            tmp, dat = inpt(dat, filt.copy())
            tmp["id"] = new_id(dat[sub])
            dat[sub] += [tmp]
            chosen += [tmp]
        elif t == exit_obj:
            if cli_ui.ask_yes_no("Data input since last save will be lost.  Confirm exit?", default=False):
                sys.exit()
            else:
                continue
        else:
            chosen += [t]
    return uniq_listofdict(chosen), dat
def uniq_listofdict(grr):
    jsoned = set([json.dumps(a,sort_keys=True) for a in grr])
    return [json.loads(a) for a in jsoned]
def filter_by_id(data, ids):
    return [d for d in data if data["id"] in ids]
def get_ids(data):
    return [d["id"] for d in data if "id" in d]
def select_fuzzy(string, items, key, limit):
    if not string:
        return []
    needle = {key: string}
    return [i[0] for i in (process.extract(needle, items, processor=lambda a: a[key], limit=limit))]
def get_by_data(data, sub, needle, inpt, key, filt):
    matches = [item for item in data[sub] if item[key] == needle]
    for k, v in filt.items():
        matches = [a for a in matches if ((k in a) and ((a[k] == v) or (v in a[k])))]
    if matches:
        return matches, data
    if cli_ui.ask_yes_no("Not found. Input new?", default=True):
        newfilt = filt
        newfilt[key] = needle
        tmp, data = inpt(data, newfilt)
        tmp["id"] = new_id(data[sub])
        data[sub] += [tmp]
        return [tmp], data
    return None, data
def input_song(data, info):
    cli_ui.info_section("New Song")

    if 'page_id' not in info:
        chosen_sources, data = choose(data, "sources", fmt_source, input_source, "title", {})
        chosen_pages = []
        for source in chosen_sources:
            cli_ui.info_2("Choosing pages from " + source["shelfmark"])
            source_chosen = []
            while True:
                if source_chosen:
                    def sort_search(a):
                        nums = re.findall(r'\d+', a["pagenum"])
                        if nums:
                            return nums[0]
                        return 0
                    source_chosen.sort(key=sort_search)
                    pagenums = [page["pagenum"] for page in source_chosen]
                    cli_ui.info_2("Currently chosen: " + ", ".join(pagenums))
                pagenum = cli_ui.ask_string("Page number (empty for no more)")
                if not pagenum:
                    break
                cp, data = get_by_data(data, "pages", pagenum, input_page, "pagenum", {"source_id": [source["id"]]})
                if cp:
                    for page in cp:
                        if page in source_chosen:
                            source_chosen.remove(page)
                            cp.remove(page)
                    source_chosen.extend(cp)
            chosen_pages += source_chosen
        info["page_id"] = get_ids(chosen_pages)
    if 'text_id' not in info or 'presentedtext' not in info:
        info["presentedtext"] = ask_multiline("Text as presented here (multiple lines)")
        new_obj = {"edited": "New"}
        exit_obj = {"edited": "Exit"}
        more_obj = {"edited": "More"}
        lim = 5
        chosen = []
        passfmt = lambda a: fmt_text(a, True if a in chosen else False)
        while True:
            choice_texts = select_fuzzy(info["presentedtext"], data["texts"], "edited", lim)
            for text in chosen:
                if text not in choice_texts:
                    choice_texts += [text]
            choice_texts += [new_obj, more_obj, exit_obj]
            t = cli_ui.ask_choice("Texts", choices=choice_texts, func_desc=passfmt, sort=False)
            if t == None:
                break
            if t in chosen:
                chosen.remove(t)
            elif t == new_obj:
                tmp, data = input_text(data, {})
                tmp["id"] = new_id(data["texts"])
                data["texts"] += [tmp]
                chosen += [tmp]
                choice_texts = [tmp] + choice_texts
            elif t == exit_obj:
                if cli_ui.ask_yes_no("Data input since last save will be lost.  Confirm exit?", default=False):
                    sys.exit()
                else:
                    continue
            elif t == more_obj:
                lim *= 2
                continue
            else:
                chosen += [t]
        info["text_id"] = get_ids(chosen)
    if 'composer_id' not in info or 'presentedcomposer' not in info:
        cli_ui.info_1("Composers")
        info["presentedcomposer"] = cli_ui.ask_string("Composer as presented (including 'by ...')")
        new_obj = {"name": "New"}
        exit_obj = {"name": "Exit"}
        more_obj = {"name": "More"}
        lim = 5
        chosen = []
        passfmt = lambda a: fmt_person(a, True if a in chosen else False)
        while True:
            if not info['presentedcomposer']:
                break
            choice_composers = select_fuzzy(info["presentedcomposer"], data["people"], "name", lim)
            for composer in chosen:
                if composer not in choice_composers:
                    choice_composers += [composer]
            choice_composers += [new_obj, more_obj, exit_obj]
            t = cli_ui.ask_choice("Composers", choices=choice_composers, func_desc=passfmt, sort=False)
            if t == None:
                break
            if t in chosen:
                chosen.remove(t)
            elif t == new_obj:
                tmp, data = input_person(data, {})
                tmp["id"] = new_id(data["people"])
                data["people"] += [tmp]
                chosen += [tmp]
                choice_composers = [tmp] + choice_composers
            elif t == exit_obj:
                if cli_ui.ask_yes_no("Data input since last save will be lost.  Confirm exit?", default=False):
                    sys.exit()
                else:
                    continue
            elif t == more_obj:
                lim *= 2
                continue
            else:
                chosen += [t]
        info["composer_id"] = get_ids(chosen)
    if 'genre_id' not in info:
        genre, data = choose(data, "genres", fmt_genre, input_genre, "name", {})
        info["genre_id"] = get_ids(genre)
    if 'title' not in info:
        info["title"] = cli_ui.ask_string("Title")
    if 'voices' not in info:
        while True:
            info["voices"] = cli_ui.ask_string("Number of voices")
            if(str(int(info["voices"])) == info["voices"]):
                break
    if 'key_id' not in info:
        key, data = choose(data, "keys", fmt_key, input_key, "name", {})
        info["key_id"] = get_ids(key)
    if 'tempo_id' not in info:
        tempo, data = choose(data, "tempos", fmt_tempo, input_tempo, "name", {})
        info["tempo_id"] = get_ids(tempo)
    if 'timesig_id' not in info:
        timesignature, data = choose(data, "timesigs", fmt_timesig, input_timesig, "name", {})
        info["timesig_id"] = get_ids(timesignature)
    if 'audio' not in info:
        cli_ui.info_1("Audio")
        if cli_ui.ask_yes_no("Is there an audio file for this song?", default=False):
            info['audio'] = input_audio()
    return info, data
def regularise_input(raw):
    def is_integer(i):
        if (type(i) == int) or ((type(i) == str) and (str(int(i)) == i)):
            return True
        else:
            if i.is_integer():
                return True
        return False
    def is_string(s):
        return isinstance(s, str) or s is None
    def is_iterable(a):
        try:
            iterator = iter(a)
        except TypeError:
            return False
        else:
            return True
    def is_array_of_integer(ia):
        if not is_iterable(ia):
            return False
        for i in ia:
            if not is_integer(i):
                return False
        return True
    def is_array_of_string(ia):
        if not is_iterable(ia):
            return False
        for i in ia:
            if not is_string(i):
                return False
        return True
    fmt = {
            "sources": {
                "id": is_integer,
                "shelfmark": is_string,
                "title": is_string,
                "formattedtitle": is_string,
                "editor_id": is_array_of_integer
            },
            "pages": {
                "id": is_integer,
                "pagenum": is_string,
                "source_id": is_array_of_integer
            },
            "people": {
                "id": is_integer,
                "name": is_string,
                "dates": is_array_of_string
            },
            "texts": {
                "id": is_integer,
                "edited": is_string,
                "character_id": is_array_of_integer,
                "warning_id": is_array_of_integer
            },
            "characters": {
                "id": is_integer,
                "name": is_string
            },
            "genres": {
                "id": is_integer,
                "name": is_string
            },
            "keys": {
                "id": is_integer,
                "name": is_string
            },
            "tempos": {
                "id": is_integer,
                "name": is_string
            },
            "songs": {
                "id": is_integer,
                "page_id": is_array_of_integer,
                "text_id": is_array_of_integer,
                "presentedtext": is_string,
                "composer_id": is_array_of_integer,
                "presentedcomposer": is_string,
                "genre_id": is_array_of_integer,
                "title": is_string,
                "voices": is_string,
                "key_id": is_array_of_integer,
                "tempo_id": is_array_of_integer,
                "timesig_id": is_array_of_integer
            }
    }
    for key, val in fmt.items():
        if key not in raw:
            raise ValueError(key + " does not exist")
        if not raw[key]:
            raw[key] = []
        for it in raw[key]:
            for fmid, fmval in val.items():
                if fmid not in it:
                    raise ValueError(key + " " + fmid + " does not exist")
                if not fmval(it[fmid]):
                    raise ValueError(key + " " + fmid + " doesn't pass test")
    return raw
def find_with_id(sub, findid):
    for item in sub:
        if item["id"] == findid:
            return item
    return None
def list_file(dat):
    sources = []
    cli_ui.info_section("Sources")
    for source in dat["sources"]:
        eds = [] 
        for edid in source["editor_id"]:
            eds += [find_with_id(dat["people"], edid)["name"]]
        t = [
                (cli_ui.bold, source["shelfmark"]),
                (cli_ui.underline, fmt_len(source["title"])),
                (cli_ui.faint, ",\n".join(eds)),
                (cli_ui.faint, ",\n".join(source["images"]))
            ]
        sources += [t]
    headers = ["Shelfmark", "Title", "Editor(s)", "Image(s)"]
    cli_ui.info_table(sources, headers=headers)
    texts = []
    cli_ui.info_section("Texts")
    for text in dat["texts"]:
        characters = []
        for chid in text["character_id"]:
            characters += [find_with_id(dat["characters"], chid)["name"]]
        t = [
                (cli_ui.faint, fmt_len(text["edited"])),
                (cli_ui.faint, ",\n".join(characters))
            ]
        texts += [t]
    headers = ["Text", "Characters"]
    cli_ui.info_table(texts, headers=headers)
    people = []
    cli_ui.info_section("People")
    for person in dat["people"]:
        t = [
            (cli_ui.bold, person["name"]),
            (cli_ui.faint, '(' + ' - '.join(person["dates"]) + ')')
        ]
        people += [t]
    headers = ["Name", "Dates"]
    cli_ui.info_table(people, headers=headers)
    songs = []
    cli_ui.info_section("Songs")
    for song in dat["songs"]:
        composers = []
        for cmpid in song["composer_id"]:
            composers += [find_with_id(dat["people"], cmpid)["name"]]
        genres = []
        for genid in song["genre_id"]:
            genres += [find_with_id(dat["genres"], genid)["name"]]
        keys = []
        for keyid in song["key_id"]:
            keys += [find_with_id(dat["keys"], keyid)["name"]]
        pages = []
        for pageid in song["page_id"]:
            page = find_with_id(dat["pages"], pageid)
            shelfmarks = []
            for sourceid in page["source_id"]:
                shelfmarks +=  [find_with_id(dat["sources"], sourceid)["shelfmark"]]
            pagenum = page["pagenum"]
            images = page["images"]
            pages += ["(" + ", ".join(shelfmarks) + ") page " + pagenum + "\n[images: " + "]\n[images: ".join(images) + "]"]
        pages.sort()
        timesigs = []
        for timesigid in song["timesig_id"]:
            timesigs += [find_with_id(dat["timesigs"], timesigid)["name"]]
        tempos = []
        for tempoid in song["tempo_id"]:
            tempos += [find_with_id(dat["tempos"], tempoid)["name"]]
        texts = []
        for textid in song["text_id"]:
            texts += [fmt_len(find_with_id(dat["texts"], textid)["edited"])]
        title = ""
        if song["title"]:
            title = song["title"]
        t = [
            (cli_ui.bold, fmt_len(title)),
            (cli_ui.faint, "\n".join(texts)),
            (cli_ui.bold, song["voices"]),
            (cli_ui.faint, ", ".join(composers)),
            (cli_ui.faint, "\n".join(pages)),
            (cli_ui.faint, ", ".join(genres)),
            (cli_ui.faint, ", ".join(keys)),
            (cli_ui.faint, ", ".join(timesigs)),
            (cli_ui.faint, ", ".join(tempos))
        ]
        songs += [t]
    headers = ["Title", "Text", "Voices", "Composer", "Source / Pages", "Genre",
            "Keys", "Timesig", "Tempo"]
    cli_ui.info_table(songs, headers=headers)
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--list", action="store_true", help="List the contents of the file")
    parser.add_argument("-i", "--images", action=readable_dir,
            default="./images/", help="Location of the manuscript images (default './images/')")
    parser.add_argument("-a", "--audio", action=readable_dir,
            default="./audio/", help="Location of the audio files (default './audio/')")
    parser.add_argument("FILE", help="filename of JSON file containing the current data. If it does not exist, it will be created")
    args = parser.parse_args()
    images_path = args.images
    audio_path = args.audio
    fname = args.FILE
    try:
        f = open(fname, "r")
    except FileNotFoundError:
        f = open(fname, "w+")
        cli_ui.error(fname + " does not exist, creating it.")
        initdat = {"sources": [], "pages": [], "people": [], "texts": [],
                "characters": [], "genres": [], "keys": [], "tempos": [],
                "songs": []}
        f.write(json.dumps(initdat, sort_keys=True, indent=4, separators=(',', ': ')))
        f.close()
        f = open(fname, "r")
    raw = json.loads(f.read())
    f.close()
    raw = regularise_input(raw)
    if args.list:
        list_file(raw)
    else:
        lastsongpage = []
        while True:
            if lastsongpage and cli_ui.ask_yes_no("Same source and page?", default=False):
                song, raw = input_song(raw, {"page_id": lastsongpage})
            else:
                song, raw = input_song(raw, {})
            lastsongpage = song["page_id"]
            song["id"] = new_id(raw["songs"])
            raw["songs"] += [song]
            f = open(fname, "w")
            f.write(json.dumps(raw, sort_keys=True, indent=4, separators=(',', ': ')))
            f.close()
            cli_ui.info("Wrote output to " + fname)
