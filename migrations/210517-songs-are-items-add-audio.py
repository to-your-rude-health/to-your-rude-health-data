import json
import sys
from copy import deepcopy
j = {}
with open('../src/tyrh.json', 'r') as fp:
    j = json.load(fp)
old_songs = deepcopy(j['songs'])
j['songs'] = []
j['items'] = []
j['audios'] = []
j['editions'] = []
def find_text(text_id, songs):
    rets = []
    for song in songs:
        if text_id in song['text_id']:
            rets.append(song)
    return rets
def find_audio(filename, audios):
    for audio in audios:
        if audio['filename'] == filename:
            return audio
    return None
song_id = 1
audio_id = 1
for item in old_songs:
    song_record = {
        "genre_id": item['genre_id'],
        "text_id": item['text_id'],
        "item_id": [item['id']],
        "hero_image": None,
        "edition_id": None
    }
    if 'audio' in item and item['audio']:
        audio_record = find_audio(item['audio'], j['audios'])
        if audio_record:
            song_record['audio_id'] = [audio_record['id']]
        else:
            title = input("Found audio '{}'. Title: ".format(item['audio']))
            j['audios'].append({
                "id": audio_id,
                "title": title,
                "filename": item['audio']
            })
            song_record['audio_id'] = [audio_id]
            audio_id += 1
    item_record = {
        "composer_id": item['composer_id'],
        "id": item['id'],
        "key_id": item['key_id'],
        "page_id": item['page_id'],
        "presentedcomposer": item['presentedcomposer'],
        "presentedtext": item['presentedtext'],
        "tempo_id": item['tempo_id'],
        "timesig_id": item['timesig_id'],
        "title": item['title'],
        "voices": item['voices']
    }
    if len(song_record['text_id']) > 1:
        print("I don't know what to do with more than one text {}".format(item['id']))
        sys.exit()
    found_text = find_text(song_record['text_id'][0], j['songs'])
    make_song = True
    for text in found_text:
        if input("Old song {} has same text as {}. Merge? (Y/n) ".format(
                item['id'], ", ".join([str(i) for i in text['item_id']]))).upper()[0]  != 'N':
            print("Merging")
            text['item_id'] = list(set(text['item_id'] + [item['id']]))
            text['genre_id'] = list(set(text['genre_id'] + item['genre_id']))
            if ('audio_id' in text and text['audio_id'] and 'audio_id' in
                    song_record and song_record['audio_id']):
                text['audio_id'] = list(set(text['audio_id'] + song_record['audio_id']))
            make_song = False
            break
    if make_song:
        song_record['id'] = song_id
        j['songs'].append(song_record)
        song_id += 1
    j['items'].append(item_record)
with open('output.json', 'w') as fp:
    json.dump(j, fp)
